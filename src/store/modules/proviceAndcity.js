const provinceAndCity= {
    state: {
        province: '',
        city: '',
        area: '',
        localDetails: ''
    },
    mutations: {
        CHANGE_PROVINCE: (state, data) => {
            state.province= data
        },
        CHANGE_CITY: (state, data) => {
            state.city= data
        },
        CHANGE_AREA: (state, data) => {
            state.area= data
        },
        CHANGE_LOCALDETAILS: (state, data) => {
            state.localDetails= data
        },
    },
    actions: {
        changeProvince({commit}, data) {
            commit('CHANGE_PROVINCE', data)
        },
        changeCity({commit}, data) {
            commit('CHANGE_CITY', data)
        },
        changeArea({commit}, data) {
            commit('CHANGE_AREA', data)
        },
        changeLocalDetails({commit}, data) {
            commit('CHANGE_LOCALDETAILS', data)
        }
    }
}
export default provinceAndCity;