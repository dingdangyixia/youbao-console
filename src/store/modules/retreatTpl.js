const retreatTpl = {
    state: {
        // 少入
        stockOnData: [], // 在途
        stockInData: [], // 入库仓
        stockOutData: [], // 出库仓
        //  多入
        stockInDataMore: [],
        editOn: true,
        editOut: false,
        editIn: false
        
    },
    mutations: {
        CHANGE_STOCK_ON_DATA: (state, data) => {
            state.stockOnData= data
        },
        CHANGE_STOCK_IN_DATA: (state, data) => {
            state.stockInData= data
        },
        CHANGE_STOCK_OUT_DATA: (state, data) => {
            state.stockOutData= data
        },
        CHANGE_STOCK_IN_DATA_MORE: (state, data) => {
            state.stockInDataMore= data
        },
        CHANGE_EDIT_ON: (state, data) => {
            state.editOn= data
        },
        CHANGE_EDIT_OUT: (state, data) => {
            state.editOut= data
        },
        CHANGE_EDIT_IN: (state, data) => {
            state.editIn= data
        },
    },
    actions: {
        changeStockOnData({commit}, data) {
            commit('CHANGE_STOCK_ON_DATA', data)
        },
        changeStockInData({commit}, data) {
            commit('CHANGE_STOCK_IN_DATA', data)
        },
        changeStockOutData({commit}, data) {
            commit('CHANGE_STOCK_OUT_DATA', data)
        },
        changeStockInDataMore({commit}, data) {
            commit('CHANGE_STOCK_IN_DATA_MORE', data)
        },
        changeEditOn({commit}, data) {
            commit('CHANGE_EDIT_ON', data)
        },
        changeEditOut({commit}, data) {
            commit('CHANGE_EDIT_OUT', data)
        },
        changeEditIn({commit}, data) {
            commit('CHANGE_EDIT_IN', data)
        }
    }
}

export default retreatTpl;