const cashSummary= {
    state: {
        page: 1,
    },
    mutations: {
        CHANGE_PAGE: (state, data) => {
            state.page= data
        },
    },
    actions: {
        changePage({commit}, data) {
            commit('CHANGE_PAGE', data)
        }
    }
}
export default cashSummary;