// 模块引入
import Vuex from 'vuex'
import "babel-polyfill"
import createLogger from 'vuex/dist/logger'
import goods from './modules/goods'
import cashSummary from './modules/cashSummary'
import provinceAndCity from './modules/proviceAndcity'
import retreatTpl from './modules/retreatTpl'

// 根目录初始值
const state= {
    isOpen: false,
    isactiviteAttribute: 'first',
    isactiviteUniqueCode: 'first',
    isActivitySettlement: 'first',
    imageArr: [],
    progress: 0,
    isLogin: false,
    userPowers: [], /**用户权限集合 */
    isChange: false ,/**用户权限变更 */
    loginGroup: null, // 用户当前登录角色
}
const mutations= {
    OPEN_MENU:(state, data) => {
		state.isOpen = data
    },
    ISACTIVITEATTRIBUTE:(state, data) => {
		state.isactiviteAttribute = data
    },
    ISACTIVITEUNIQUECODE:(state, data) => {
		state.isactiviteUniqueCode = data
    },
    ISACTIVITYSETTLEMENT:(state, data) => {
		state.isActivitySettlement = data
    },
    UPLOADIMAGES: (state, data) => {
        state.imageArr=data 
    },
    // excel下载进度
    CHANGE_PROGRESS: (state, data) => {
        state.progress= data
    },
    //登录
    IS_LOGIN: (state, data) => {
        state.isLogin= data
    },
    /**权限变更 */
    IS_CHANGE: (state, data) => {
        state.isChange= data
    },
    /**用户权限集合 */
    USER_POWERS: (state, data) => {
        state.userPowers= data
    },
    LOGIN_GROUP: (state, data) => {
        state.loginGroup= data
    }


}
const actions= {
    openMenu({
        commit
    }, data) {
        commit('OPEN_MENU', data)
    },
    isActiviteAttribute({
        commit
    }, data) {
        commit('ISACTIVITEATTRIBUTE', data)
    },
    isActiviteUniqueCode({
        commit
    }, data) {
        commit('ISACTIVITEUNIQUECODE', data)
    },
    isActivitySettlement({
        commit
    }, data) {
        commit('ISACTIVITYSETTLEMENT', data)
    },
    uploadImages({commit}, data) {
        commit ('UPLOADIMAGES', data)
    },
    changeProgress({commit}, data) {
        commit('CHANGE_PROGRESS', data)
    },
    isLogin({commit}, data) {
        commit('IS_LOGIN', data)
    },
    isChange({commit}, data) {
        commit('IS_CHANGE', data)
    },
    userPowers({commit}, data) {
        commit('USER_POWERS', data)
    },
    loginGroup({commit}, data) {
        commit('LOGIN_GROUP', data)
    }

}
const logger =  createLogger({
    collapsed: true, // 自动展开记录的 mutation
    filter (mutation, stateBefore, stateAfter) {
        // 若 mutation 需要被记录，就让它返回 true 即可
        // 顺便，`mutation` 是个 { type, payload } 对象
        return mutation.type !== "aBlacklistedMutation"
    },
    transformer (state) {
        // 在开始记录之前转换状态
        // 例如，只返回指定的子树
        return state.subTree
    },
    mutationTransformer (mutation) {
        // mutation 按照 { type, payload } 格式记录
        // 我们可以按任意方式格式化
        return mutation.type
    },
    logger: console, // 自定义 console 实现，默认为 `console`
})
export default new Vuex.Store({
    modules: {
        goods,
        cashSummary,
        provinceAndCity,
        retreatTpl
    },

    state,
    mutations,
    actions,
    plugins: process.env.NODE_ENV == 'development' ? [createLogger()] : []
})