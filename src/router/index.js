import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/index.js'

import Home from '../components/commonComponents/home'
import Login from '../components/commonComponents/login'
/** 商品管理 */
import GoodsList from '../components/goodsManagement/goods/goodsList'
import AttributeMaintenance from '../components/goodsManagement/attributes/attributeMaintenance'
import CategoryList from '../components/goodsManagement/category/categoryList'

/** 库存管理 */
import UniqueCodeList from '../components/stockManagement/uniqueCode/uniqueCodeList'
import StockList from '../components/stockManagement/stockManage/stockList'
import AllocationTaskList from '../components/stockManagement/allocationManage/allocationTask/allocationTaskList' // 调拨任务
import AllocationExamineList from '../components/stockManagement/allocationManage/allocationExamine/allocationExamineList' // 调拨审核
import PickingList from '../components/stockManagement/pickingManage/pickingList' // 拣货任务
import InventoryList from '../components/stockManagement/inventoryManagement/inventoryList' // 盘库列表
import InventoryPromise from '../components/stockManagement/inventoryManagement/inventoryPromise' // 问题追踪
/**退库管理 */
import RetreatList from '../components/stockManagement/retreatManagement/retreatList' // 单据列表
import RetreatTrack from '../components/stockManagement/retreatManagement/retreatTrack' // 仓入追踪
import RetreatApprove from '../components/stockManagement/retreatManagement/retreatApprove' // 单据审批

/** 系统管理 */
import ModulesList from '../components/systemManagement/systemModules/modulesList'
import RolesList from '../components/systemManagement/systemRoles/rolesList'
import UsersList from '../components/systemManagement/systemUsers/usersList'
import StoresList from '../components/systemManagement/systemStores/storesList'
import GroupsList from '../components/systemManagement/systemGroups/groupsList'
/**财务管理 */
import FinancialReconciliation from '../components/financialManagement/financialReconciliation'
import CashFlow from '../components/financialManagement/cashFlow'
import CashSummary from '../components/financialManagement/cashSummary'
import StoreSettlement from '../components/financialManagement/storeSettlement/storeSettlement'
/**门店仓库 */
import StoreList from '../components/storeAndwarehouseManagement/storeManagement/storeList'
import WarehouseList from '../components/storeAndwarehouseManagement/warehouseManagement/warehouseList'
import SalesMonitor from '../components/storeAndwarehouseManagement/storeOperate/salesMonitor'
import StoreRemoveStock from '../components/storeAndwarehouseManagement/storeOperate/storeRemoveStock'
import SalesExhibition from '../components/storeAndwarehouseManagement/storeOperate/salesExhibition'

Vue.use(Router)

const router = new Router({
    routes: [

        {
            path: '/',
            redirect: '/login',
        },
        {
            path: '/home',
            name: 'home',
            component: Home,
            meta: {}
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {}
        },
        /** 商品管理 */
        {
            path: '/goodsManagement/goods/goodsList',
            name: 'goodsList',
            component: GoodsList,
            meta: {
                parentName: '商品管理',
                title: '商品列表',
                name: '商品资料'
            }
        },
        {
            path: '/goodsManagement/attributes/attributeMaintenance',
            name: 'attributeMaintenance',
            component: AttributeMaintenance,
            meta: {
                parentName: '商品管理',
                title: '属性列表',
                name: '属性维护',
                tabList: [{
                        id: 'first',
                        label: '基础属性'
                    },
                    {
                        id: 'second',
                        label: '自定义属性'
                    }
                ]
            }
        },
        {
            path: '/goodsManagement/category/categoryList',
            name: 'categoryList',
            component: CategoryList,
            meta: {
                parentName: '商品管理',
                title: '类目列表',
                name: '类目管理'
            }
        },
        /** 库存管理 */
        /**唯一码 */
        {
            path: '/stockManagement/uniqueCode/uniqueCodeList',
            name: 'uniqueCodeList',
            component: UniqueCodeList,
            meta: {
                parentName: '库存管理',
                title: '入库单列表',
                name: '入库管理',
            }
        },
        {
            path: '/stockManagement/stockManage/stockList',
            name: 'stockList',
            component: StockList,
            meta: {
                parentName: '库存管理',
                title: '库存管理',
                name: '库存管理',
            }
        },
        {
            path: '/stockManagement/allocationManage/allocationTask/allocationTaskList',
            name: 'allocationTaskList',
            component: AllocationTaskList,
            meta: {
                parentName: '库存管理',
                title: '调拨列表',
                name: '调拨管理',
            }
        },
        {
            path: '/stockManagement/allocationManage/allocationExamine/allocationExamineList',
            name: 'allocationExamineList',
            component: AllocationExamineList,
            meta: {
                parentName: '库存管理',
                title: '审核列表',
                name: '调拨审核',
            }
        },
        {
            path: '/stockManagement/pickingManage/pickingList',
            name: 'pickingList',
            component: PickingList,
            meta: {
                parentName: '库存管理',
                title: '出库列表',
                name: '出库管理',
            }
        },
        {
            path: '/stockManagement/inventoryManagement/inventoryList',
            name: 'inventoryList',
            component: InventoryList,
            meta: {
                parentName: '库存管理',
                title: '盘库列表',
                name: '盘库管理',
            }
        },
        {
            path: '/stockManagement/inventoryManagement/inventoryPromise',
            name: 'inventoryPromise',
            component: InventoryPromise,
            meta: {
                parentName: '库存管理',
                title: '问题追踪',
                name: '盘库管理',
            }
        },
        {
            path: '/stockManagement/retreatManagement/retreatList',
            name: 'retreatList',
            component: RetreatList,
            meta: {
                parentName: '库存管理',
                title: '单据列表',
                name: '退库管理',
            }
        },
        {
            path: '/stockManagement/retreatManagement/retreatApprove',
            name: 'retreatApprove',
            component: RetreatApprove,
            meta: {
                parentName: '库存管理',
                title: '单据审批',
                name: '退库管理',
            }
        },
        {
            path: '/stockManagement/retreatManagement/retreatTrack',
            name: 'retreatTrack',
            component: RetreatTrack,
            meta: {
                parentName: '库存管理',
                title: '仓入追踪',
                name: '退库管理',
            }
        },



        /**系统管理 */
        {
            path: '/systemManagement/systemModules/modulesList',
            name: 'modulesList',
            component: ModulesList,
            meta: {
                parentName: '系统管理',
                title: '模块列表',
                name: '模块管理',
            }
        },
        {
            path: '/systemManagement/systemRoles/rolesList',
            name: 'rolesList',
            component: RolesList,
            meta: {
                parentName: '系统管理',
                title: '角色列表',
                name: '角色管理',
            }
        },
        {
            path: '/systemManagement/systemUsers/usersList',
            name: 'usersList',
            component: UsersList,
            meta: {
                parentName: '系统管理',
                title: '用户列表',
                name: '用户管理',
            }
        },
        {
            path: '/systemManagement/systemStores/storesList',
            name: 'storesList',
            component: StoresList,
            meta: {
                parentName: '系统管理',
                title: '门店列表',
                name: '门店管理',
            }
        },
        {
            path: '/systemManagement/systemGroups/groupsList',
            name: 'groupsList',
            component: GroupsList,
            meta: {
                parentName: '系统管理',
                title: '分组列表',
                name: '分组管理',
            }
        },
        /**财务管理 */
        /**财务对账 */
        {
            path: '/financialManagement/financialReconciliation',
            name: 'financialReconciliation',
            component: FinancialReconciliation,
            meta: {
                parentName: '财务管理',
                title: '财务对账',
                // name: '财务对账',
            }
        },
        /**打款流水 */
        {
            path: '/financialManagement/cashFlow',
            name: 'cashFlow',
            component: CashFlow,
            meta: {
                parentName: '财务管理',
                title: '打款流水',
                // name: '打款流水',
            }
        },
        /**打款汇总 */
        {
            path: '/financialManagement/cashSummary',
            name: 'cashSummary',
            component: CashSummary,
            meta: {
                parentName: '财务管理',
                title: '打款汇总',
                // name: '打款汇总',
            }
        },
        /**门店结算 */
        {
            path: '/financialManagement/storeSettlement',
            name: 'storeSettlement',
            component: StoreSettlement,
            meta: {
                parentName: '财务管理',
                title: '门店结算',
                // name: '属性维护',
                tabList: [{
                        id: 'first',
                        label: '结算单'
                    },
                    {
                        id: 'second',
                        label: '物流明细'
                    }
                ]
            }
        },

        /**
         * 门店仓库
         */
        /**门店管理 */
        {
            path: '/storeManagement/storeList',
            name: 'storeList',
            component: StoreList,
            meta: {
                parentName: '门店运营',
                title: '门店列表',
                name: '门店管理',
            }
        },
        /**仓库管理 */
        {
            path: '/warehouseManagement/warehouseList',
            name: 'warehouseList',
            component: WarehouseList,
            meta: {
                parentName: '库存管理',
                title: '仓库列表',
                name: '仓库管理',
            }
        },
        /**销售监控 */
        {
            path: '/storeOperate/SalesMonitor',
            name: 'salesMonitor',
            component: SalesMonitor,
            meta: {
                parentName: '门店运营',
                title: '销售监控',
            }
        },
        /**门店消存 */
        {
            path: '/storeOperate/storeRemoveStock',
            name: 'storeRemoveStock',
            component: StoreRemoveStock,
            meta: {
                parentName: '门店运营',
                title: '门店销存',
            }
        },
        /*销售看板*/
        {
            path: '/storeOperate/salesExhibition',
            name: 'salesExhibition',
            component: SalesExhibition,
            meta: {
                parentName: '门店运营',
                title: '销售看板',
            }
        },
        {
            path: '*',
            redirect: '/',
            hidden: true
        }

    ]
})
router.beforeEach((to, from, next) => {
    try {
        let userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
        let userInfos = JSON.parse(sessionStorage.getItem('userInfos'));
        let userPower = userInfos ? userInfos.userPower : null;
        var power = []
        var filterGroupIds = function(arr) {
            let arr1 = []
            for (let i = 0; i < arr.length; i++) {
                if (arr[i].children.length > 0) {
                    arr1 = arr1.concat(this.filterGroupIds(arr[i].children))
                } else {
                    arr1.push(arr[i].groupId)
                }
            }
            return arr1
        }

        if (userInfo && userInfo != null && userInfo != undefined) {
            next()
        } else {
            if (to.path !== '/login') {
                next('/login')
            } else {
                next()
            }
        }
    } catch (err) {
        next('/login')
    }
})

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

export default router;