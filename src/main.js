// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import promise from 'es6-promise';
import "babel-polyfill"
import server from './api/servers/index.js';
import md5 from 'md5';
import paramsError from './api/commonJs/paramsError';

import store from './store/index';
import Options from './api/json/selectOptions';
import './assets/font/iconfont.css';
// import moment from 'moment';
import directives from './api/commonJs/directives';


//promise.polyfill();

Vue.config.productionTip = false
// api

Vue.use(directives)
Vue.prototype.$server= server
Vue.prototype.$paramsError= paramsError
// md5
Vue.prototype.$md5= md5
Vue.prototype.$Options= Options
// Vue.prototype.$Moment= moment


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
}).$mount('#app')
