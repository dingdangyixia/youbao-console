// 菜单集合
const Menu = [{
        permissionId: '0-1',
        title: '销售看板',
        icon: 'iconfont xiaoshoukanban',
        httpPath: '/storeOperate/salesExhibition',
        children: []
    },
    {
        permissionId: '1-1',
        title: '商品管理',
        icon: 'iconfont goods1',
        httpPath: '/goodsManagement',
        children: [{
                permissionId: '1-1-1',
                title: '商品资料',
                httpPath: '/goodsManagement/goods/goodsList',
                children: []
            },
            {
                permissionId: '1-1-2',
                title: '类目管理',
                httpPath: '/goodsManagement/category/categoryList',
                children: []
            },
            {
                permissionId: '1-1-3',
                title: '属性维护',
                httpPath: '/goodsManagement/attributes/attributeMaintenance',
                children: []
            }
        ]
    },
    {
        permissionId: '1-2',
        title: '库存管理',
        icon: 'iconfont stock',
        httpPath: '/stockManagement',
        children: [
            {
                permissionId: '1-2-0',
                title: '仓库管理',
                httpPath: '/warehouseManagement/warehouseList',
                children: []
            },
            {
                permissionId: '1-2-1',
                title: '入库管理',
                httpPath: '/stockManagement/uniqueCode/uniqueCodeList',
                children: []
            },
            {
                permissionId: '1-2-2',
                title: '调拨管理',
                httpPath: '/stockManagement/allocationManage/allocationTask/allocationTaskList',
                children: []
            },
            {
                permissionId: '1-2-3',
                title: '调拨审核',
                httpPath: '/stockManagement/allocationManage/allocationExamine/allocationExamineList',
                children: []
            },
            {
                permissionId: '1-2-4',
                title: '出库管理',
                httpPath: '/stockManagement/pickingManage/pickingList',
                children: []
            },
            {
                permissionId: '1-2-5',
                title: '盘库管理',
                icon: 'iconfont mendianyunying',
                httpPath: '/stockManagement/inventoryManagement',
                children: [{
                        permissionId: '1-2-5-1',
                        title: '盘库列表',
                        httpPath: '/stockManagement/inventoryManagement/inventoryList',
                        children: []
                    },
                    {
                        permissionId: '1-2-5-2',
                        title: '问题追踪',
                        httpPath: '/stockManagement/inventoryManagement/inventoryPromise',
                        children: []
                    }
                ]
            },
            {
                permissionId: '1-2-6',
                title: '退库管理',
                icon: 'iconfont mendianyunying',
                httpPath: '/stockManagement/retreatManagement',
                children: [{
                        permissionId: '1-2-6-1',
                        title: '单据列表',
                        httpPath: '/stockManagement/retreatManagement/retreatList',
                        children: []
                    },
                    {
                        permissionId: '1-2-6-2',
                        title: '单据审批',
                        httpPath: '/stockManagement/retreatManagement/retreatApprove',
                        children: []
                    },
                    {
                        permissionId: '1-2-6-3',
                        title: '仓入追踪',
                        httpPath: '/stockManagement/retreatManagement/retreatTrack',
                        children: []
                    }
                ]
            },
        ]
    },
    {
        permissionId: '1-4',
        title: '财务管理',
        icon: 'iconfont finance',
        httpPath: '/financialManagement',
        children: [{
                permissionId: '1-4-1',
                title: '财务对账',
                httpPath: '/financialManagement/financialReconciliation',
                children: []
            },
            {
                permissionId: '1-4-2',
                title: '打款流水',
                httpPath: '/financialManagement/cashFlow',
                children: []
            },
            {
                permissionId: '1-4-3',
                title: '打款汇总',
                httpPath: '/financialManagement/cashSummary',
                children: []
            },
            {
                permissionId: '1-4-4',
                title: '门店结算',
                httpPath: '/financialManagement/storeSettlement',
                children: []
            }
        ]
    },
    
    {
        permissionId: '1-6',
        title: '门店运营',
        icon: 'iconfont operate',
        httpPath: '/storeOperate',
        children: [
            {
                permissionId: '1-3-1-1',
                title: '门店管理',
                httpPath: '/storeManagement/storeList',
                children: []  
            },
            {
                permissionId: '1-6-1',
                title: '销售监控',
                httpPath: '/storeOperate/salesMonitor',
                children: []
            },
            {
                permissionId: '1-6-2',
                title: '门店销存',
                httpPath: '/storeOperate/storeRemoveStock',
                children: []
            }
        ]
    },
    {
        permissionId: '1-3',
        title: '系统管理',
        icon: 'iconfont system',
        httpPath: '/systemManagement',
        children: [{
                permissionId: '1-3-1',
                title: '模块管理',
                httpPath: '/systemManagement/systemModules/modulesList',
                children: []
            },
            {
                permissionId: '1-3-2',
                title: '角色管理',
                httpPath: '/systemManagement/systemRoles/rolesList',
                children: [

                ]
            },
            {
                permissionId: '1-3-3',
                title: '用户管理',
                httpPath: '/systemManagement/systemUsers/usersList',
                children: []
            },

            {
                permissionId: '1-3-5',
                title: '分组管理',
                httpPath: '/systemManagement/systemGroups/groupsList',
                children: []
            },
            
        ]
    },
]

export default Menu;
/**
 * 调拨
 * 1. 调拨任务名称 =》 调拨单
 * 2.新建出入门店校验（互斥）
 * 3.申请数量取消默认、
 * 4.提交页面添加显示信息（门店、申请人、创建时间）
 * 5.拣货详情取消右侧list
 * 6.拣货单已发货详情信息展示
 * 
 */