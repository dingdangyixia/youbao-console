import httpRequest from '../../commonJs/http';

/**查看省市区api */
const provinceCityApi= 'youbao/store/api/area/list'
/**仓库api */
const commonWarehouseApi= 'youbao/stock/api/stock-warehouse/warehouse'
/**门店api */
const commonStoreApi= 'youbao/store/api/store'
/**获取聚水潭分仓 */
const warehousesApi= 'youbao/stock/api/jsterp/warehouses'
/**查询所有可用仓库列表 */
const getAllWarehouseApi= 'youbao/stock/api/stock-warehouse/warehouse/selected'

const storeWarehouseController= {
    /**获取聚水潭 */
    getJsterpWarehousesList: async () => {
        let data= await httpRequest.get(warehousesApi);
        return data;
    },
    /**获取省市区 */
    getProvinceCity: async (id) => {
        let params= {
            pid: id
        }
        let data= await httpRequest.get(provinceCityApi, {params});
        return data;
    },
    /**仓库 */
    getWarehouseList: async (params) => {
        let data= await httpRequest.get(commonWarehouseApi, {params});
        return data;
    },
    createWarehouse: async (params) => {
        let data= await httpRequest.post(commonWarehouseApi, params);
        return data;
    },
    checkWarehouseDetail: async (id) => {
        let data= await httpRequest.get(commonWarehouseApi +'/' +id);
        return data;
    },
    editWarehouse: async (id, params) => {
        let data= await httpRequest.put(commonWarehouseApi +'/' +id, params);
        return data;
    },
    deleteWarehouse: async (id) => {
        let data= await httpRequest.delete(commonWarehouseApi +'/' +id);
        return data;
    },

    /**门店 */
    createStore: async (params) => {
        let data= await httpRequest.post(commonStoreApi, params);
        return data;
    },
    editStore: async (params) => {
        let data= await httpRequest.put(commonStoreApi, params);
        return data;
    },
    getStoreList: async (params) => {
        let data= await httpRequest.get(commonStoreApi+ '/list', {params});
        return data;
    },
    getStoreDetails: async (id) => {
        let params= {
            storeNo: id
        }
        let data= await httpRequest.get(commonStoreApi + '/info', {params});
        return data;
    },
    /**获取所有可用仓库列表 */
    getAllWarehouseList: async () => {
        let data= await httpRequest.get(getAllWarehouseApi);
        return data;
    }

}
export default storeWarehouseController;