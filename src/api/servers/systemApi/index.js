import httpRequest from '../../commonJs/http'
/**
 * 接口地址
 */
/**优爆v1按省份获取门店 */
const getStoreListByGroupApi = 'ddaigo-platform/store/storeListGroupByCity'

/** v2 */
const systemStore = 'youbao/stock/api/stock-warehouse/warehouse'

/**系统用户接口api */
const commonUserApi = 'youbao/system/api/user'
    /** 模块公共api地址 */
const commonModuleApi = 'youbao/system/api/permission'
    /**角色公共api */
const commonRoleApi = 'youbao/system/api/role'
    /**
     * 组
     */
    /**分组公共api */
const commonGroupApi = 'youbao/system/api/group'
    /** 新建仓库 */
const systemControlApi = {
    getStoreListByGroup: async() => {
        let params = {
            data: {
                name: ''
            }
        }
        let data = await httpRequest.post(getStoreListByGroupApi, params);
        return data;
    },

    getSystemStoresList: async(params) => {
        let data = await httpRequest.get(systemStore, { params });
        return data;
    },
    createStore: async(params) => {
        let data = await httpRequest.post(systemStore, params);
        return data;
    },
    /**
     * 登录
     */
    loginControl: async(params) => {
        let data = await httpRequest.post(loginApi, params);
        return data;
    },

    /**
     * 系统用户
     */
    /**获取平台用户- 所有 */
    getUserList: async(params) => {
        let data = await httpRequest.get(commonUserApi + '/all', { params });
        return data;
    },
    /**获取用户列表-分页 */
    getUserListByPage: async(params) => {
        let data= await httpRequest.get(commonUserApi + '/users', {params});
        return data;
    },
    /**创建用户 */
    createUser: async(params) => {
        let data = await httpRequest.post(commonUserApi, params);
        return data;
    },
    /**编辑用户 */
    editUser: async(params) => {
        let data = await httpRequest.put(commonUserApi, params);
        return data;
    },
    /**启用&停用用户 */
    changeUserStatus: async(params) => {
        let data = await httpRequest.patch(commonUserApi + '/lock', params);
        return data;
    },
    /**删除/恢复用户 */
    deleteOrRecoveryUser: async (params) => {
        let data= await httpRequest.patch(commonUserApi + '/delete', params);
        return data;
    },
    /**获取用户信息 */
    getUserDetailById: async(id) => {
        let data = await httpRequest.get(commonUserApi + '/info/' + id);
        return data;
    },
    /**用户添加组角色 */
    addGroupRoleByUser: async(params) => {
        let data = await httpRequest.post(commonUserApi + '/groupandrole', params);
        return data;
    },
    /**获取用户分组角色信息 */
    getUserGroupRoleDetails: async(id) => {
        let data = await httpRequest.get(commonUserApi + '/grouproles/' + id);
        return data;
    },

    // /** 用户添加角色分组 v2 */ 12-02
    addGroupRoleForUser:  async (params) => {
        let data= await httpRequest.post(commonUserApi + '/roleandgroups', params);
        return data;
    },
    getUserGroupRoleDetailsV2: async (id) => {
        let data= await httpRequest.get(commonUserApi + '/roleandgroups' + '?userId=' + id)
        return data;
    },
    /**删除用户角色 */
    deleteUserGroup: async (params) => {
        let data= await httpRequest.delete(commonUserApi + '/roleandgroups', {params})
        return data;
    },
    /**        -------------------------           */
    
    /**删除用户分组角色 */
    deleteUserGroupRole: async(id) => {
        let data = await httpRequest.delete(commonUserApi + '/groupandrole/' + id);
        return data;
    },
    /**是否启用用户数据权限 */
    isisAssignGroup: async (params) => {
        let data= await httpRequest.put(commonRoleApi + '/isassigngroup', params);
        return data;
    },

    /*权限管理*/
    /*获取模块列表*/
    getPermissionList: async() => {
        let data = await httpRequest.get(commonModuleApi + '/list');
        return data;
    },
    /*获取模块详情*/
    getPermissionById: async(id) => {
        let data = await httpRequest.get(commonModuleApi + '/info/' + id);
        return data;
    },
    /*创建模块*/
    addPermission: async(params) => {
        let data = await httpRequest.post(commonModuleApi, params);
        return data;
    },
    /*删除模块*/
    deletePermission: async(params) => {
        let data = await httpRequest.patch(commonModuleApi + '/delete', params);
        return data;
    },
    /*禁用模块*/
    lockPermission: async(params) => {
        let data = await httpRequest.patch(commonModuleApi + '/lock', params);
        return data;
    },
    /*修改模块*/
    editPermission: async(params) => {
        let data = await httpRequest.put(commonModuleApi, params);
        return data;
    },

    /**
     * 角色
     */
    /**获取角色列表 */
    getSystemRoleList: async(params) => {
        let data = await httpRequest.get(commonRoleApi + '/list', { params });
        return data;
    },
    /**新建角色 */
    createSystemRole: async(params) => {
        let data = await httpRequest.post(commonRoleApi, params);
        return data;
    },
    /**启用、禁用角色 */
    changeRoleStatus: async(params) => {
        let data = await httpRequest.patch(commonRoleApi + '/lock', params);
        return data;
    },
    /**角色添加权限 */
    systemRoleBindModule: async(params) => {
        let data = await httpRequest.post(commonRoleApi + '/permission', params);
        return data;
    },
    /**查看角色信息 */
    getRoleDetails: async(id) => {
        let data = await httpRequest.get(commonRoleApi + '/info/' + id);
        return data;
    },
    /**角色信息编辑 */
    editRoleDetails: async(params) => {
        let data = await httpRequest.put(commonRoleApi, params);
        return data;
    },

    /**获取分组列表 */
    getSystemGroupsList: async() => {
        let data = await httpRequest.get(commonGroupApi + '/list');
        return data;
    },
    /**添加分组 */
    addSystemGroup: async (params) => {
        let data= await httpRequest.post(commonGroupApi, params);
        return data;
    }
}

export default systemControlApi;