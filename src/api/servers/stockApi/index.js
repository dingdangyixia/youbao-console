import httpRequest from '../../commonJs/http'

/**入库单查询 */
const stockInListApi= 'youbao/stock/api/stockin/orders'
/**入库单详情查询 */
const stockInDetailApi= 'youbao/stock/api/stockin/order/'
/**调拨单列表 */
const allocationListApi= 'youbao/stock/api/stock/transfers'
/** 调拨公用接口地址*/
const commonAllocationApi= 'youbao/stock/api/stock/transfer'
/**获取商品 */
const getStockGoodsListApi= 'youbao/stock/api/stock/goods/list'

/**
 * 拣货单
 */
/**拣货公用api */
const commonPickingApi= '';
/**拣货单列表 */
const pickingListApi= 'youbao/stock/api/stock-out-order/outorder'
/**拣货单详情 */
const pickingDetailsApi= 'youbao/stock/api/stock-out-order/outorder/'
/**获取拣货单物流信息 */
const getLogisticByIdApi= 'youbao/stock/api/stock-out-order/outorder/delivery/'
/**拣货单商品 */
const pickingDetailGoodsApi= 'youbao/stock/api/stock-out-detail/detail/'
/**添加物流信息 */
const createExpressApi= 'youbao/stock/api/stock-delivery-order/delivery'

/**
 * 盘库管理
 */

const commonInventoryApi= "youbao/stock/api/stock/count/order"
/**获取盘点单品牌（盘点单进度详情） */
const inventoryBrandApi= 'youbao/stock/api/stock/count/order/balance/plan/'

/**获取仓库内所有品牌 */
const getWarehouseAllBrandsApi= 'youbao/stock/api/stock/brand/list/'

/**
 * 退库管理
 */
/**退库模块公用api */
const commonRetreatApi= 'youbao/stock/api/stock-return-order/'


const stockControlApi= {
    /**入库单列表 */
    getStockInList: async (query, params) => {
        let data= await httpRequest.post(stockInListApi+ '?page='+query.page+ '&size=' +query.size, params)
        return data;
    },
    /**入库单详情 */
    getStockInDeatil: async (id, params) => {
        let data= await httpRequest.get(stockInDetailApi+ id+ '/details',{params})
        return data;
    },
    /**获取调拨任务列表 */
    getAllocationList: async (query, params) => {
        let data= await httpRequest.post(allocationListApi + '?page=' + query.page + '&size=' + query.size, params);
        return data;
    },
    /**删除调拨任务 */
    delAllocation: async (id) => {
        let data= await httpRequest.delete(commonAllocationApi+'/'+id);
        return data;
    },
    /**新建调拨任务 */
    createAllocation: async (params) => {
        let data= await httpRequest.post(commonAllocationApi, params);
        return data;
    },
    /**查看调拨任务 （编辑）*/
    editAllocationDetails: async (id, params) => {
        let data= await httpRequest.get(commonAllocationApi+'/detail/'+ id, {params});
        return data;
    },
    /**删除调拨任务商品 */
    delAllocationGoods: async (id) => {
        let data= await httpRequest.delete(commonAllocationApi+ '/detail/' + id);
        return data;
    },
    /**获取商品 */
    getStockGoodsList: async (query, params) => {
        let data= await httpRequest.post(getStockGoodsListApi+ '?page=' + query.page + '&size=' + query.size +'&transferId=' + query.transferId, params);
        return data;
    },
    /**商品校验 */
    checkAllocationGoods: async(params) => {
        let data= await httpRequest.get(commonAllocationApi+'/detail', {params});
        return data;
    },
    /**调拨任务添加商品 */
    addAllocationGoods: async (params) => {
        let data= await httpRequest.post(commonAllocationApi+'/detail', params);
        return data;
    },
    /**调拨任务修改商品数量 */
    modifyAllocationGoodsNum: async (params) => {
        let data= httpRequest.put(commonAllocationApi+ '/detail', params);
        return data;
    },
    /**调拨任务提交 */
    allocationSubmit: async (id) => {
        let data= await httpRequest.put(commonAllocationApi + '/'+ id);
        return data;
    },
    /**查看调拨任务 */
    checkAllocationDetails: async (id) => {
        let data= await httpRequest.get(commonAllocationApi+ '/detail/info/' +id);
        return data;
    },
    getAllocationGoodsByBrandCode: async (query) => {
        let data= await httpRequest.get(commonAllocationApi+ '/detail/info/?transferId=' + query.transferId + '&brandCode=' + query.brandCode);
        return data;
    },
    /**调拨任务审核 */
    changeAllocationStatus: async (params) => {
        let data= await httpRequest.put(commonAllocationApi+ '/list', params);
        return data;
    },
    /**调拨审核列表 */
    getAllocationAuditList: async (query, params) => {
        let data= await httpRequest.post(commonAllocationApi+ '/list/?page='+query.page+ '&size='+query.size, params);
        return data;
    },
    /**获取调拨单内已添加商品 */
    getAllocationGoodsList: async (id) => {
        let data= await httpRequest.get(commonAllocationApi+'/details/' +id);
        return data;
    },

    /**
     * 拣货部分
     */
    /**拣货单列表 */
    getPickingList: async (query, params) => {
        let data= await httpRequest.post(pickingListApi +'?page=' + query.page + '&size=' + query.size, params);
        return data;
    },
    /**拣货单详情 */
    getPickingDetails: async (id) => {
        let data= await httpRequest.get(pickingDetailsApi + id);
        return data;
    },
    /**获取拣货单 */
    getPickingDetailGoodsList: async (id, query) => {
        let data= await httpRequest.get(pickingDetailGoodsApi+'/'+id +'?page=' +query.page + '&size=' +query.size);
        return data;
    },
    /**获取拣货单物流信息 */
    getPickingExpress: async (id) => {
        let data= await httpRequest.get(getLogisticByIdApi+id);
        return data;
    },
    /**创建物流信息 */
    createExpress: async (params) => {
        let data= await httpRequest.post(createExpressApi, params);
        return data;
    },

    /**
     * 盘库管理
     */
    /**获取仓库内所有品牌 */
    getWarehouseAllBrands: async (id) => {
        let data= await httpRequest.get(getWarehouseAllBrandsApi + id);
        return data;
    },
    /**盘点单列表 */
    getInventoryList: async (params) => {
        let data= await httpRequest.get(commonInventoryApi +'/list', {params});
        return data;
    },
    /**新建盘点任务 */
    createInventory: async (params) => {
        let data= await httpRequest.post(commonInventoryApi, params);
        return data;
    },
    /**获取盘点单品牌 */
    getBrandListById: async (id) => {
        let data= await httpRequest.get()
    },
    /**盘点单删除 */
    delInventoryById: async (id) => {
        let data= await httpRequest.delete(commonInventoryApi+ '/' +id);
        return data;
    },
    /**查询盘点单品牌 */
    getBrandById: async (id) => {
        let data= await httpRequest.get(inventoryBrandApi + id);
        return data;
    },


    /**
     * 退库
     */
    /**退库列表 */
    getRetreatList: async (query, params) => {
        let data= await httpRequest.post(commonRetreatApi +'list?page=' + query.page + '&size=' +query.size, params);
        return data;
    },
    /**退库单详情 */
    getRetreatDetail: async (id) => {
        let data= await httpRequest.get(commonRetreatApi + 'detail/pc/' +id);
        return data;
    },
    /**提交退库单 */
    submitRetreatOrder: async (params) => {
        let data= await httpRequest.put(commonRetreatApi + 'commit', params);
        return data;
    },
    /**仓退列表 */
    warehouseOrderList: async (params) => {
        let data= await httpRequest.post(commonRetreatApi + 'list/warehouse', params);
        return data;
    },
    /**退库单审批 */
    retreatApprove: async (params) => {
        let data= await httpRequest.put(commonRetreatApi+ 'approve', params);
        return data;
    }
}
export default stockControlApi;