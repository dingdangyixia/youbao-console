import httpRequest from '../../commonJs/http'

/**
 * 接口地址
 */
/**登录接口 */
const systemLogin= 'youbao/system/api/login';
/**获取用户组角色 */
const getUserGroupsRoleApi= 'youbao/system/api/user/control/grouproles/';
/**获取用户权限 */
const getUserPowerApi= 'youbao/system/api/user/control/permissions/';
/**用户修改密码 */
const resetPasswordApi= 'youbao/system/api/user/password';

/**获取用户所有角色 12-02 */
const getUserRolesApi= 'youbao/system/api/user/control/roles'


const loginAuthorityControlApi= {
    /**登录 */
    systemLogin: async (params) => {
        let data= await httpRequest.post(systemLogin, params);
        return data;
    },
    /**获取用户组 */
    getUserGroupsRole: async (id, systemId) => {
        let data= await httpRequest.get(getUserGroupsRoleApi + '?userId=' + id + '&systemId=' +systemId);
        return data;
    },
    /**获取角色权限 */
    getUserPower: async (systemId, id) => {
        let data= await httpRequest.get(getUserPowerApi + '?systemId=' +systemId + '&userAssignId=' +id);
        return data;
    },
    /**登录密码修改 */
    resetPassword: async (params) => {
        let data= await httpRequest.patch(resetPasswordApi, params);
        return data;
    },
    /**获取用户角色 */
    getUserRoles: async (params) => {
        let data= await httpRequest.get(getUserRolesApi, {params});
        return data;
    }
}

export default loginAuthorityControlApi;