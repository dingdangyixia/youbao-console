import formate from '../commonJs/formate.js'
import goodsControlApi from './goodsApi'
import stockControlApi from './stockApi'
import uploadApi from './upload'
import excelApi from './excelApi'
import systemControlApi from './systemApi'
import loginAuthorityControlApi from './loginApi'
import financialControlApi from './financialApi'
import storeWarehouseController from './storeWarehouseApi'
import salesExhibitionApi from './storeOperate'

export default {
    // formate,
    goodsControlApi,
    stockControlApi,
    uploadApi,
    excelApi,
    systemControlApi,
    loginAuthorityControlApi,
    financialControlApi,
    storeWarehouseController,
    salesExhibitionApi
}