import httpRequest from '../../commonJs/http'
/**
 * 销售看板 */
const getKeys = '/youbao/report/api/overview/sum/keyindex' //销售概览指标查询
const getDaySales = '/youbao/report/api/overview/sum/bydaily' // 每日销量情况查询
const getStoreSales = '/youbao/report/api/overview/sum/bystore' //门店销量情况查询
const getAreaSales = '/youbao/report/api/overview/top/byregional' //地区销量排行榜查询
const getPrices = '/youbao/report/api/overview/sum/byprice' //价格带销存对比查询
const getCategory = '/youbao/report/api/overview/top/bycategory' //品类销量排行榜查询
const getBrand = '/youbao/report/api/overview/top/bybrand' //品牌销量排行榜查询
const getGoods = '/youbao/report/api/overview/top/bygoods' //商品销量排行榜查询

/*门店消存*/
const salenstock = '/youbao/report/api/storeview/sum/salenstock' //门店销售情况查询
const dailysale = '/youbao/report/api/storeview/sum/dailysale' //门店销售情况查询
    /*销售监控*/
const storekeys = '/youbao/report/api/storeview/sum/keyindex' //门店关键指标查询
const poorsale = '/youbao/report/api/storeview/sum/poorsale' //销售较差品牌查询
const zerosale = '/youbao/report/api/storeview/sum/zerosale' //销售为零商品查询

/* 门店列表 */
const getStore = '/youbao/report/api/dimension/stores'




const salesExhibitionApi = {
    /** 商品列表api */
    getKeys: async(params) => {
        let data = await httpRequest.post(getKeys, params)
        return data;
    },
    getDaySales: async(params) => {
        let data = await httpRequest.post(getDaySales, params)
        return data;
    },
    getStoreSales: async(params) => {
        let data = await httpRequest.post(getStoreSales, params)
        return data;
    },
    getAreaSales: async(params) => {
        let data = await httpRequest.post(getAreaSales, params)
        return data;
    },
    getPrices: async(params) => {
        let data = await httpRequest.post(getPrices, params)
        return data;
    },
    getCategory: async(params) => {
        let data = await httpRequest.post(getCategory, params)
        return data;
    },
    getBrand: async(params) => {
        let data = await httpRequest.post(getBrand, params)
        return data;
    },
    getGoods: async(params) => {
        let data = await httpRequest.post(getGoods, params)
        return data;
    },
    getDailysale: async(params) => {
        let data = await httpRequest.post(dailysale, params)
        return data;
    },
    getSalenstock: async(pageInfo, params) => {
        let data = await httpRequest.post(salenstock + `?page=${pageInfo.page}&size=${pageInfo.size}`, params)
        return data;
    },
    getStorekeys: async(pageInfo, params) => {

        let data = await httpRequest.post(storekeys + `?page=${pageInfo.page}&size=${pageInfo.size}`, params)
        return data;
    },
    getPoorsale: async(pageInfo, params) => {
        let data = await httpRequest.post(poorsale + `?page=${pageInfo.page}&size=${pageInfo.size}`, params)
        return data;
    },
    getZerosale: async(pageInfo, params) => {
        let data = await httpRequest.post(zerosale + `?page=${pageInfo.page}&size=${pageInfo.size}`, params)
        return data;
    },
    getStore: async(params) => {
        let data = await httpRequest.get(getStore, params)
        return data;
    },
}

export default salesExhibitionApi;