import httpRequest from '../../commonJs/http'
/**门店收款明细 */
const storeReceiptChannelApi= 'ddaigo-platform/finances/storeReceiptChannel';
/**获取对账单列表接口 */
const financialListApi= 'youbao/finance/api/finance-receivable/list';
/**打款汇总 */
const cashSummaryApi= 'youbao/finance/api/finance-payment/list';
/**打款流水接口 */
const cashFlowApi= 'youbao/finance/api/finance-payment/pay/list';
/**未打款记录 */
const noRecordOfPaymentApi= 'youbao/finance/api/finance-payment/pay/list/noPaid'
const getCashFlowDateApi= 'youbao/finance/api/finance-payment/pay/list/date'
/**第三方订单流水导出 */
const cashFlowDownloadApi ='youbao/finance/api/finance-payment/transaction/export'

/**
 * 门店结算 */

/**门店结算列表 */
const storeSettlementApi= 'youbao/finance/api/finance-settle-order/list/pc'
/**获取门店科目分成 v1 */
const storeIntoApi= 'ddaigo-platform/statistics/storeSettles'
/**查询费用 */
const expenseApi= 'youbao/finance/api/finance-settle-order/expense'
/**保存/修改结算单 */
const saveSettlementApi= 'youbao/finance/api/finance-settle-order/order'


/**
 * 物流相关
 */

/**物流公用api */
const commonLogisticApi= 'youbao/finance/api/finance/express/'


/**获取周列表 */
const getWeekApi= 'youbao/system/api/date/finance/weeks'

const financialControlApi = {
    /**v1 门店收款渠道明细 */
    storeReceiptChannel: async (params) => {
        let data= await httpRequest.post(storeReceiptChannelApi, params);
        return data;
    },
    /**对账单列表 */
    getFinancialList: async (params) => {
        let data= await httpRequest.get(financialListApi, {params});
        return data;
    },
    /**对账单统计 */
    getFinancialStatic: async (params) => {
        let data= await httpRequest.get(financialListApi + '/total', {params});
        return data;
    },
    /**打款汇总  按日*/
    getCashSummaryList: async(params) => {
        let data= await httpRequest.get(cashSummaryApi + '/day', {params});
        return data;
    },
    /**打款汇总  按周*/
    getCashSummaryListByWeek: async(params) => {
        let data= await httpRequest.get(cashSummaryApi + '/week', {params});
        return data;
    },
    /**打款汇总  按周*/
    getCashSummaryListByMonth: async(params) => {
        let data= await httpRequest.get(cashSummaryApi + '/month', {params});
        return data;
    },
    /**获取打款流水列表 */
    getCashFlowList: async (params) => {
        let data= await httpRequest.get(cashFlowApi, {params});
        return data;
    },
    /**获取未打款记录 */
    getNoRecordOfPaymentApiList: async (params) => {
        let data= await httpRequest.get(noRecordOfPaymentApi, {params});
        return data;
    },
    /**查询打款日期 */
    getCashFlowDate: async(params) => {
        let data= await httpRequest.get(getCashFlowDateApi, {params});
        return data;
    },
    /**第三方流水下载 */
    cashFlowDownload: async (params) => {
        let data= await httpRequest.get(cashFlowDownloadApi, {params});
        return data;
    },
    /**获取当前年份所有周 */
    /**获取门店结算单列表 */
    getStoreSettlementList: async (params) => {
        let data= await httpRequest.get(storeSettlementApi, {params});
        return data;
    },
    /**获取周列表 */
    getWeekList: async (params) => {
        let data= await httpRequest.get(getWeekApi, {params});
        return data;
    },
    /**新建结算单 */
    createSettlement: async (params) => {
        let data= await httpRequest.post(storeSettlementApi, params);
        return data;
    },
    /**编辑 获取分成 v1 */
    getStoreInto: async (params) => {
        let data= await httpRequest.post(storeIntoApi, params);
        return data;
    },
    /**查询费用明细 */
    getExpenseList: async (params) => {
        let data= await httpRequest.get(expenseApi,{params});
        return data;
    },
    /**保存结算单 */
    saveSettlement: async (params) => {
        let data= await httpRequest.post(saveSettlementApi, params);
        return data;
    },
    /**修改结算单状态 */
    changeSettlementStatus: async (params) => {
        let data= await httpRequest.put(saveSettlementApi, params);
        return data;
    },
    /**保存后查看结算单详情 */
    checkSettlementDetails: async (id) => {
        let data= await httpRequest.get(saveSettlementApi+'/' +id);
        return data;
    },

    /**
     * 物流
     */
    /**物流上传 */
    logisticUpload: async (query, params) => {
        let data= await httpRequest.post(commonLogisticApi +'upload' + '?startDate=' +query.startDate + '&endDate=' + query.endDate +'&uploadUser=' + query.uploadUser+ '&uploadUserId=' + query.uploadUserId, params);
        return data;
    },
    /**物流上传记录列表 */
    logisticUploadList: async (params) => {
        let data= await httpRequest.get(commonLogisticApi +'records', {params});
        return data;
    },
    /**删除明细 */
    deleteLogistic: async (id) => {
        let data= await httpRequest.delete(commonLogisticApi + id);
        return data;
    }
    

};
export default financialControlApi;