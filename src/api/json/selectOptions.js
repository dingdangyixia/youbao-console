const Options= {
    // 门店列表
    storeList: [
        {
            "city": "保定市",
            "cityId": 130600,
            "list": [
                {
                    "storeNo": "S00000002",
                    "storeName": "易县1店",
                    "cityId": 130600,
                    "city": "保定市",
                    "province": "河北省"
                }
            ]
        }
    ],
    // 门店类型 v1
    storeTypes: [
        {
            value: 0,
            label: '全部'
        },
        {
            value: 1,
            label: '加盟店'
        },
        {
            value: 2,
            label: '合作店'
        },{
            value: 3,
            label: '会员店'
        }
    ],
    /**v2 */
    storeType: [
        // {
        //     value: 0,
        //     label: '直营店'
        // },
        {
            value: 1,
            label: '加盟店'
        },
        {
            value: 2,
            label: '合作店'
        },{
            value: 3,
            label: '会员店'
        }
    ],
    // 门店状态
    commonStatus: [
        {
            value: '0',
            label: '全部'
        },
        {
            value: '1',
            label: '无效'
        },
        {
            value: '2',
            label: '有效'
        }
    ],
    // 调拨状态
    allocationStatus: [
        {
            value: '0',
            label: '全部'
        },
        {
            value: '1',
            label: '待提交'
        },
        {
            value: '2',
            label: '待审核'
        },
        {
            value: '3',
            label: '已拒绝'
        },
        {
            value: '4',
            label: '待出库'
        }
        // {
        //     value: '5',
        //     label: '已完成'
        // }
    ],
    // 拣货状态
    pickingStatus: [
        {
            value: '',
            label: '全部'
        },
        {
            value: 0,
            label: '待拣货'
        },
        {
            value: 1,
            label: '待发货'
        },
        {
            value: 2,
            label: '已发货'
        }
    ],
    /**门店结算 */
    settlementStatus: [
        {
            label: '全部',
            value: 4
        },
        {
            label: '待提交',
            value: 0
        },
        {
            label: '待确认',
            value: 1
        },
        {
            label: '待结算',
            value: 2
        },
        {
            label: '已结算',
            value: 3
        }
    ]
}
export default Options;