/* 
    try{}catch(err){} 捕捉前端网络请求传参 错误信息封装 错误码：0000/前端错误
*/
import {Message} from 'element-ui'
const paramsError= (error) => {

    if(process.env.NODE_ENV == 'development') {
        let type= 'warning'
    
        let errorLoading=  new Message({
            message: '0000: '+ error.message ? error.message : error,
            type: type,
            duration: 2000,
            showClose: true
        })
        return errorLoading;
    }else{
        console.log(error)
    }
}

export default paramsError;