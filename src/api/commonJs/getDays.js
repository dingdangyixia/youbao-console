/**
 *
 *
 * @export getDay
 * @param {Date} day
 * @param {'yyyy-MM-dd hh:mm:ss'} fmt
 * @returns
 */
export function getDay(day, fmt) {
    var today = new Date();

    var targetday_milliseconds = today.getTime() + 1000 * 60 * 60 * 24 * day;

    today.setTime(targetday_milliseconds); //注意，这行是关键代码

    var tYear = today.getFullYear();
    var tMonth = today.getMonth();
    var tDate = today.getDate();

    var o = {
        "M+": doHandleMonth(tMonth + 1), //月份
        "d+": doHandleMonth(tDate), //日
        "h+": today.getHours(), //小时
        "m+": today.getMinutes(), //分
        "s+": today.getSeconds(), //秒
        "q+": Math.floor((today.getMonth() + 3) / 3), //季度
        "S": today.getMilliseconds() //毫秒
    };

    fmt = fmt ? fmt : "yyyy/MM/dd";

    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (tYear + "").substr(4 - RegExp.$1.length));

    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));

    return fmt;

}

function doHandleMonth(month) {
    var m = month;
    if (month.toString().length == 1) {
        m = "0" + month;
    }
    return m;
}

//获取最近7天日期
// getDay(0);//当天日期
// getDay(-7);//7天前日期