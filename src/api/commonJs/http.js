/** 网络请求封装 包含请求成功失败拦截 loading加载关闭 */

import axios from 'axios'
import{Message} from 'element-ui'
import {showLoading, hideLoading} from './loading'
import router from '../../router'
import store from '../../store'
import server from '../servers/index'

/** axios 封装 */
const service = axios.create({
    baseURL: process.env.BASE_API, // api的base_url
    headers: {
        'Content-Type': 'application/json'
    },
   
    timeout: 30000, // 网络请求超时 30s
    onUploadProgress: function (progressEvent) {  // `onUploadProgress` 允许为上传处理进度事件
        //console.log(progressEvent, 'progressEvent')
        // Do whatever you want with the native progress event
    },
    onDownloadProgress: function (progressEvent) { // `onDownloadProgress` 允许为下载处理进度事件
       // console.log(progressEvent, 'progressEvent')
        // 对原生进度事件的处理
    },
})

// 请求拦截器
service.interceptors.request.use(
    config => {
        let urls= config.url.split('/')
        if(urls[0] == 'youbao') {
            let sessionId= sessionStorage.getItem('sessionId')
            if(sessionId) {
                config.headers.sessionId= sessionId
            }
        }else{
            config.headers.sessionId= 'eyJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOjJ9._ZUB9LlikWZknaetvmOq3-aQYKyyMY_zedd80JRYiUU'
        }
        
        showLoading();
        return config;
    },
    error => {
        hideLoading();
        return Promise.error(error);
    }
)

// 请求成功信息
const successHandle= (msg) => {
    
    if(msg && msg != undefined) {
        Message({
            message: msg,
            type: 'success',
            duration: 2000,
            showClose: true
        })
    }
}

// 错误信息
const errorHandle = (code, other) => {
    var userAssignId= sessionStorage.getItem('userAssignId')
    // 状态码判断
    switch (code) {
        
        case '108':
            Message({
                message: '用户会话已过期，请重新登录',
                type: 'error',
                duration: 2000,
                showClose: true
            })
            setTimeout(() => {
                router.push({name: 'login'})
            }, 2000)
            break;
        // 500 sessionID 认证错误
        case '500':
            Message({
                message: '认证失败',
                duration: 2000,
                type: 'error',
                showClose: true
            });
           
            break;
            
        case '111': 
            Message({
                message: '用户权限已变更',
                type: 'waring',
                duration: 2000,
                showClose: true
            })
            store.dispatch('isChange', true)
             /**权限变更 */
            server.loginAuthorityControlApi.getUserPower(1, userAssignId).then(res => {
                store.dispatch('userPowers', res.data)
            })
            
            break;
        // 其他错误，直接抛出错误提示
        default:
            Message({
                message: other,
                duration: 2000,
                type: 'error',
                showClose: true
            });
    }
}

// 响应拦截器
service.interceptors.response.use(
    // 请求成功
    res => {
        let data= res.data
        if(data.code == 200 ||(data.hash && data.key)) {
           // successHandle(data.message) 成功提示暂时关闭
           // showSuccessMessage(data.message)
            hideLoading();
            return Promise.resolve(data)
        }else{
            errorHandle(data.code, data.message)
     
            hideLoading();
            return Promise.reject(data)
        }
    },
    // 请求失败
    error => {
        const { response } = error;
        if (response) {
            // 请求已发出，但是不在2xx的范围
            errorHandle(response.status, response.data.message);
            hideLoading()
            return Promise.reject(response);
        } else {
            Message({
                message: '请求服务失败',
                type: 'error',
                duration: 2000,
                showClose: true
            })
            hideLoading()
        }
    });
export default service;
